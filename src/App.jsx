import React, { useState, useEffect } from 'react';
import './App.css';

function App() {
  const [currencies, setCurrencies] = useState([]);
  const [fromCurrency, setFromCurrency] = useState('BOB');
  const [toCurrency, setToCurrency] = useState('USD');
  const [amount, setAmount] = useState(1);
  // const [conversionRate, setConversionRate] = useState(null);
  const [convertedAmount, setConvertedAmount] = useState(null);

  useEffect(() => {    
    const fetchData = async () => {
      try {
        const response = await fetch('https://api.apilayer.com/currency_data/list', {
          method: 'GET', 
          redirect: 'follow',
          headers: {
            // 'Authorization': 'Bearer YOUR_JWT_TOKEN',
            'apikey': 'NkAf8IO3vuu8WG8PG36r4d68RPJ196cd'
          },
        });

        if (!response.ok) {
          throw new Error('Error en la solicitud');
        }

        const responseData = await response.json();
        const elementos = Object.entries(responseData.currencies).map(([clave, valor]) => {
          return {
            key: clave,
            value: valor
          };
        });
        
        setCurrencies(elementos);        
        console.log(elementos);

      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchData();

  }, [fromCurrency, toCurrency]);

  const handleAmountChange = (event) => {
    setAmount(event.target.value);
    // handleConvert();
  };

  const handleFromCurrencyChange = (event) => {
    setFromCurrency(event.target.value);
    // handleConvert();
  };

  const handleToCurrencyChange = (event) => {
    setToCurrency(event.target.value);
    // handleConvert();
  };

  const handleConvert = async () => {
    //
    try {
      const response = await fetch(`https://api.apilayer.com/currency_data/convert?to=${toCurrency}&from=${fromCurrency}&amount=${amount}`, {
        method: 'GET', 
        redirect: 'follow',
        headers: {
          // 'Authorization': 'Bearer YOUR_JWT_TOKEN',
          'apikey': 'NkAf8IO3vuu8WG8PG36r4d68RPJ196cd'
        },
      });

      if (!response.ok) {
        throw new Error('Error en la solicitud');
      }

      const responseData = await response.json();
      
      
      setConvertedAmount(responseData.result);        
      console.log(responseData);

    } catch (error) {
      console.error('Error:', error);
    }
    
  };

  return (
    <div className="App">      
      <div className='App-header'>
        <h3>Conversor de Divisas</h3>
          <div className="input-container">
            <input type="number" value={amount} onChange={handleAmountChange} min={0} step={0.25}/>
            <select value={fromCurrency} onChange={handleFromCurrencyChange}>
              {currencies.map((currency) => (
                <option key={currency.key} value={currency.key}>
                  {currency.value}
                </option>
              ))}
            </select>
            &nbsp;
            a
            &nbsp;
            <select value={toCurrency} onChange={handleToCurrencyChange}>
              {currencies.map((currency) => (
                <option key={currency.key} value={currency.key}>
                  {currency.value}
                </option>
              ))}
            </select> 
            &nbsp;
            <button onClick={handleConvert}>Convertir</button>
          </div>
          
          <div className="read-the-docs">            
            <p> {amount} {fromCurrency} = {convertedAmount !== null ? convertedAmount +' '+ toCurrency: null } </p>
          </div>
      </div>      
    </div>
  );
}

export default App;

# Examen Reactj - Conversor de Divisas

Autor: Franz Javier Muraña Cruz

## Ejecutar proyecto

Ejecute el siguiente comando en directorio raiz del proyecto:

### `npm start`

## Proyecto Demo

El demo del proyecto está disponible en el siguiente enlace:

[https://conversor-de-divisas.onrender.com/](https://conversor-de-divisas.onrender.com/)
